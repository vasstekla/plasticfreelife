﻿using AutoMapper;
using PlasticFreeLife.DTO;
using PlasticFreeLife.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlasticFreeLife.Services
{
    public class AutomappingProfiles : Profile
    {
        public AutomappingProfiles()
        {
            CreateMap<Task<UserDTO>, Task<User>>();
            CreateMap<Task<RoleDTO>, Task<Role>>();
            CreateMap<Task<PostDTO>, Task<Post>>();
            CreateMap<Task<ProductDTO>, Task<Product>>();
            CreateMap<Task<PurchaseDTO>, Task<Purchase>>();
            CreateMap<Task<FriendDTO>, Task<Friend>>();

            CreateMap<UserDTO, User>();
            CreateMap<RoleDTO, Role>();
            CreateMap<PostDTO, Post>();
            CreateMap<ProductDTO, Product>();
            CreateMap<PurchaseDTO, Purchase>();
            CreateMap<FriendDTO, Friend>();
        }
    }
}
