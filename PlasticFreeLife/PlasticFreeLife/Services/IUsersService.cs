﻿using PlasticFreeLife.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlasticFreeLife.Services
{
  public interface IUsersService
  {
    Task<int> InsertAsync(UserDTO user);
    Task<IEnumerable<UserDTO>> GetAllUsers();
    Task<UserDTO> Authenticate(AuthenticateUserDTO authenticateUser);
    Task<bool> Update(UserDTO user);
    Task<bool> Delete(int userId);
    Task<UserDTO> GetById(int userId);
    Task<UserDTO> GetByUsername(String username);
  }
}
