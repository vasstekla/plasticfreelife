﻿using AutoMapper;
using PlasticFreeLife.DAL;
using PlasticFreeLife.Models;
using PlasticFreeLife.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlasticFreeLife.Services
{
    public class PurchasesService : IPurchasesService
    {
        internal IGenericRepository<Purchase> genericRepository;
        internal IMapper mapper;
        internal IAuthentificate authenthificate;

        public PurchasesService(IMapper mapper, IGenericRepository<Purchase> genericRepository)
        {
            this.genericRepository = genericRepository;
            this.mapper = mapper;
        }


        public async Task<IEnumerable<PurchaseDTO>> GetAllPurchases()
        {
            return await mapper.Map<Task<IEnumerable<PurchaseDTO>>>(genericRepository.GetEntities());
        }

        public async Task<PurchaseDTO> GetById(int purchaseId)
        {
            return await mapper.Map<Task<PurchaseDTO>>(genericRepository.GetEntityById(purchaseId));
        }

        public async Task<IEnumerable<PurchaseDTO>> GetMyPurchases(string userId)
        {
            return await mapper.Map<Task<IEnumerable<PurchaseDTO>>>(genericRepository.GetEntities(p => p.BuyerId == Int32.Parse(userId)));
        }

        public async Task<int> Insert(PurchaseDTO purchase)
        {
            purchase.DateTime = DateTime.Now;
            Purchase entityPurchase = mapper.Map<Purchase>(purchase);
            await genericRepository.InsertEntity(entityPurchase);
            return await Task.FromResult(entityPurchase.PurchaseId);
        }
    }
}
