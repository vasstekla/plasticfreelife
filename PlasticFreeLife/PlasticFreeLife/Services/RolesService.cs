﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using PlasticFreeLife.Context;
using PlasticFreeLife.DAL;
using PlasticFreeLife.DTO;
using PlasticFreeLife.Models;

namespace PlasticFreeLife.Services
{
  public class RolesService : IRolesService
  {
    internal IGenericRepository<Role> genericRepository;
    internal IMapper mapper;

    public RolesService(IMapper mapper, DatabaseContext databaseContext)
    {
      this.genericRepository = new GenericRepository<Role>(databaseContext);
      this.mapper = mapper;
    }

    public async Task<bool> Delete(int roleId)
    {
      if(await genericRepository.GetEntityById(roleId) == null)
      {
        return await Task.FromResult(false);
      }
      await genericRepository.DeleteEntity(roleId);
      return await Task.FromResult(await genericRepository.GetEntityById(roleId) == null);
    }

    public async Task<IEnumerable<RoleDTO>> GetAllRoles()
    {
      return await mapper.Map<Task<IEnumerable<RoleDTO>>>(genericRepository.GetEntities());
    }

    public async Task<RoleDTO> GetById(int roleId)
    {
      return await mapper.Map<Task<RoleDTO>>(genericRepository.GetEntityById(roleId));
    }

    public async Task<int> Insert(RoleDTO role)
    {
      Role entityRole = mapper.Map<Role>(role);
      await genericRepository.InsertEntity(entityRole);
      return await Task.FromResult(entityRole.RoleId);
    }

    public async Task<bool> Update(RoleDTO role)
    {
      Role entityRole = mapper.Map<Role>(role);
      await genericRepository.UpdateEntity(entityRole, (int)role.RoleId);
      return await Task.FromResult(entityRole.RoleId != 0);
    }
  }
}
