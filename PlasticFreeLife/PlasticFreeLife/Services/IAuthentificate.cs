﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlasticFreeLife.Services
{
  public interface IAuthentificate
  {
    string HashPassword(string password);
    Task<int> HandleAuthentification(string newPassword, string databasePassword);
    string UnwrapTokenRole(string token, byte[] key);
    string UnwrapTokenName(string token, byte[] key);
  }
}
