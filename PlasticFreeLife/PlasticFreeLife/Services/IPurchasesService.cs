﻿using PlasticFreeLife.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlasticFreeLife.Services
{
    public interface IPurchasesService
    {
        Task<int> Insert(PurchaseDTO purchase);
        Task<IEnumerable<PurchaseDTO>> GetAllPurchases();
        Task<IEnumerable<PurchaseDTO>> GetMyPurchases(string userId);
        Task<PurchaseDTO> GetById(int purchaseId);
    }
}
