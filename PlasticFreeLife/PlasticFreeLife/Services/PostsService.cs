﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Extensions.Options;
using PlasticFreeLife.DAL;
using PlasticFreeLife.DTO;
using PlasticFreeLife.Models;

namespace PlasticFreeLife.Services
{
  public class PostsService : IPostsService
  {
    internal IGenericRepository<Post> genericRepository;
    internal IMapper mapper;
    internal IAuthentificate authenthificate;

    public PostsService(IMapper mapper, IGenericRepository<Post> genericRepository)
    {
      this.genericRepository = genericRepository;
      this.mapper = mapper;
    }


    public async Task<bool> Delete(int postId, string userId, string userRole)
    {
      Post databasePost = await genericRepository.GetEntityById(postId);
      if (databasePost.AuthorId == Int32.Parse(userId) || Int32.Parse(userRole) == 3 || Int32.Parse(userRole) == 2)
      {
        await genericRepository.DeleteEntity(postId);
        return await Task.FromResult(await genericRepository.GetEntityById(postId) == null);
      }
      return await Task.FromResult(false);

    }

    public async Task<IEnumerable<PostDTO>> GetAllPosts()
    {
      return await mapper.Map<Task<IEnumerable<PostDTO>>>(genericRepository.GetEntities());
    }

    public async Task<IEnumerable<PostDTO>> GetAllPosts(string category, string status)
    {
      if(category != null && status != null)
      {
        return await mapper.Map<Task<IEnumerable<PostDTO>>>(genericRepository.GetEntities(p => p.Status == status && p.Category == category));
      } else if (category != null) {
        return await mapper.Map<Task<IEnumerable<PostDTO>>>(genericRepository.GetEntities(p => p.Category == category));
      } else {
        return await mapper.Map<Task<IEnumerable<PostDTO>>>(genericRepository.GetEntities(p => p.Status == status));
      }
    }

    public async Task<PostDTO> GetById(int postId)
    {
      return await mapper.Map<Task<PostDTO>>(genericRepository.GetEntityById(postId));
    }

    public async Task<int> Insert(PostDTO post, string userRole)
    {
      if(Int32.Parse(userRole) == 1)
      {
        post.Status = "Pending";
      }
      post.CreateDate = DateTime.Now;
      Post entityPost = mapper.Map<Post>(post);
      await genericRepository.InsertEntity(entityPost);
      return await Task.FromResult(entityPost.PostId);

    }

    public async Task<bool> Update(PostDTO post, string userId, string userRole)
    {
      Post entityPost = mapper.Map<Post>(post);
      Post databasePost = await genericRepository.GetEntityById(post.PostId);
      if (databasePost.AuthorId == Int32.Parse(userId) || Int32.Parse(userRole) == 3 || Int32.Parse(userRole) == 2)
      {
        if (Int32.Parse(userRole) == 1)
        {
          entityPost.Status = databasePost.Status;
        }
          await genericRepository.UpdateEntity(entityPost, (int)post.PostId);
        return await Task.FromResult(entityPost.PostId != 0);
      }
      return await Task.FromResult(false);
    }
  }
}
