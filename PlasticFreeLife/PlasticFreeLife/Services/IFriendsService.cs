﻿using PlasticFreeLife.DTO;
using PlasticFreeLife.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlasticFreeLife.Services
{
    public interface IFriendsService
    {
        Task<bool> Insert(FriendDTO friend);
        Task<IEnumerable<FriendDTO>> GetAllFriends();
        Task<IEnumerable<FriendDTO>> GetMyFriends(string userId);
        Task<bool> Delete(FriendDTO friend, string userId, string userRole);
    }
}
