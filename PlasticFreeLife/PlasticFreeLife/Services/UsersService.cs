﻿using AutoMapper;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using PlasticFreeLife.Context;
using PlasticFreeLife.DAL;
using PlasticFreeLife.DTO;
using PlasticFreeLife.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace PlasticFreeLife.Services
{
  public class UsersService : IUsersService
  {
    internal IGenericRepository<User> genericRepository;
    internal IMapper mapper;
    internal IAuthentificate authenthificate;
    internal AppSettings appSettings;

    public UsersService(IMapper mapper, IGenericRepository<User> genericRepository, IOptions<AppSettings> appSettings, IAuthentificate authentificate)
    {
      this.genericRepository = genericRepository;
      this.mapper = mapper;
      this.appSettings = appSettings.Value;
      this.authenthificate = authentificate;
    }

    public async Task<UserDTO> Authenticate(AuthenticateUserDTO authenticateUser)
    {
      var user = await GetByUsername(authenticateUser.Username);
      if (user != null)
      {
        if (await authenthificate.HandleAuthentification(authenticateUser.Password, user.Password) == 1)
        {
          var tokenHandler = new JwtSecurityTokenHandler();
          var key = Encoding.ASCII.GetBytes(appSettings.Secret);
          var tokenDescriptor = new SecurityTokenDescriptor
          {
            Subject = new ClaimsIdentity(new Claim[]
              {
                    new Claim(ClaimTypes.Name, user.UserId.ToString()),
                    new Claim(ClaimTypes.Role, user.RoleId.ToString())
              }),
            Expires = DateTime.UtcNow.AddDays(7),
            SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
          };
          var token = tokenHandler.CreateToken(tokenDescriptor);
          user.Token = tokenHandler.WriteToken(token);
          user.Password = null;       
          return await Task.FromResult<UserDTO>(await mapper.Map<Task<UserDTO>>(Task.FromResult(user)));
        };
      }
      return await Task.FromResult<UserDTO>(null);
    }

    public async Task<bool> Delete(int userId)
    {
      if(await genericRepository.GetEntityById(userId) == null)
      {
        return await Task.FromResult(false);
      }
      await genericRepository.DeleteEntity(userId);
      return await Task.FromResult(await genericRepository.GetEntityById(userId) == null);
    }

    public async Task<IEnumerable<UserDTO>> GetAllUsers()
    {      
      return await mapper.Map<Task<IEnumerable<UserDTO>>>(genericRepository.GetEntities());
    }

    public async Task<UserDTO> GetById(int userId)
    {
      return await mapper.Map<Task<UserDTO>>(genericRepository.GetEntityById(userId));
    }

    public async Task<UserDTO> GetByUsername(string username)
    {
      IEnumerable<User> users = await genericRepository.GetEntities(u => u.Username == username);
      return await mapper.Map<Task<UserDTO>>(Task.FromResult(users.FirstOrDefault()));
    }

    public async Task<int> InsertAsync(UserDTO user)
    {
      IEnumerable<User> users = await genericRepository.GetEntities(u => u.Username == user.Username);
      if(users.Count() == 0)
      {
        user.Password = authenthificate.HashPassword(user.Password);
        if (user.RoleId == 0)
        {
          user.RoleId = 1;
        }
        User entityUser = mapper.Map<User>(user);
        await genericRepository.InsertEntity(entityUser);
        return await Task.FromResult(entityUser.UserId);
      }
      return await Task.FromResult<int>(-1);
    }

    public async Task<bool> Update(UserDTO user)
    {
      if (user.RoleId == 0)
      {
        user.RoleId = 1;
      }
      User entityUser = mapper.Map<User>(user);
      await genericRepository.UpdateEntity(entityUser, (int)user.UserId);
      return await Task.FromResult(entityUser.UserId != 0);
    }
  }
}
