﻿using PlasticFreeLife.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlasticFreeLife.Services
{
  public interface IPostsService
  {
    Task<int> Insert(PostDTO post, string userRole);
    Task<IEnumerable<PostDTO>> GetAllPosts();
    Task<IEnumerable<PostDTO>> GetAllPosts(string category, string status);
    Task<bool> Update(PostDTO post, string userId, string userRole);
    Task<bool> Delete(int postId, string userId, string userRole);
    Task<PostDTO> GetById(int postId);
  }
}
