﻿using PlasticFreeLife.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlasticFreeLife.Services
{
    public interface IRolesService
    {
        Task<int> Insert(RoleDTO role);
        Task<IEnumerable<RoleDTO>> GetAllRoles();
        Task<bool> Update(RoleDTO role);
        Task<bool> Delete(int roleId);
        Task<RoleDTO> GetById(int roleId);
    }
}
