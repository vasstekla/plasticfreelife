﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace PlasticFreeLife.Services
{
  public class Authentificate : IAuthentificate
  {
    public string HashPassword(string password)
    {
      byte[] salt;
      new RNGCryptoServiceProvider().GetBytes(salt = new byte[16]);
      var pbkdf2 = new Rfc2898DeriveBytes(password, salt, 10000);
      byte[] hash = pbkdf2.GetBytes(20);
      byte[] hashBytes = new byte[36];
      Array.Copy(salt, 0, hashBytes, 0, 16);
      Array.Copy(hash, 0, hashBytes, 16, 20);
      return Convert.ToBase64String(hashBytes);
    }

    public async Task<int> HandleAuthentification(string newPassword, string databasePassword)
    {
      string savedPasswordHash = databasePassword;
      byte[] hashBytes = Convert.FromBase64String(savedPasswordHash);
      byte[] salt = new byte[16];
      Array.Copy(hashBytes, 0, salt, 0, 16);

      var pbkdf2 = new Rfc2898DeriveBytes(newPassword, salt, 10000);
      byte[] hash = pbkdf2.GetBytes(20);

      for (int i = 0; i < 20; i++)
        if (hashBytes[i + 16] != hash[i])
          return await Task.FromResult<int>(-1);
      return await Task.FromResult<int>(1);
    }

    private ClaimsPrincipal UnwrapToken(string token, byte[] key)
    {
      var handler = new JwtSecurityTokenHandler();
      var validations = new TokenValidationParameters
      {
        ValidateIssuerSigningKey = true,
        IssuerSigningKey = new SymmetricSecurityKey(key),
        ValidateIssuer = false,
        ValidateAudience = false
      };
      ClaimsPrincipal returnValue;
      try
      {
        returnValue = handler.ValidateToken(token, validations, out var tokenSecure);
      }
      catch (Exception error)
      {
        return null;
      }
      return returnValue;
    }

    public string UnwrapTokenRole(string token, byte[] key)
    {
      ClaimsPrincipal claim = UnwrapToken(token, key);
      if (claim != null)
      {
        return claim.Claims.Where(c => c.Type == ClaimTypes.Role).Select(c => c.Value).SingleOrDefault();
      }
      return null;
    }

    public string UnwrapTokenName(string token, byte[] key)
    {
      ClaimsPrincipal claim = UnwrapToken(token, key);
      if (claim != null)
      {
        return claim.Identity.Name;
      }
      return null;
    }
  }
}
