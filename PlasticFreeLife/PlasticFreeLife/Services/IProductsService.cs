﻿using PlasticFreeLife.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlasticFreeLife.Services
{
    public interface IProductsService
    {
        Task<int> Insert(ProductDTO product);
        Task<IEnumerable<ProductDTO>> GetAllProducts();
        Task<IEnumerable<ProductDTO>> GetAllProducts(string category, string status, double minPrice, double maxPrice);
        Task<IEnumerable<ProductDTO>> GetMyProducts(string userId);
        Task<bool> Update(ProductDTO product, string userId, string userRole);
        Task<bool> Delete(int productId, string userId, string userRole);
        Task<ProductDTO> GetById(int productId);
    }
}
