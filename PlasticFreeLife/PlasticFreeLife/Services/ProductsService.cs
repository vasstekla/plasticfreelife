﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using PlasticFreeLife.DAL;
using PlasticFreeLife.DTO;
using PlasticFreeLife.Models;

namespace PlasticFreeLife.Services
{
    public class ProductsService : IProductsService
    {
        internal IGenericRepository<Product> genericRepository;
        internal IMapper mapper;
        internal IAuthentificate authenthificate;

        public ProductsService(IMapper mapper, IGenericRepository<Product> genericRepository)
        {
            this.genericRepository = genericRepository;
            this.mapper = mapper;
        }

        public async Task<bool> Delete(int productId, string userId, string userRole)
        {

            Product databaseProduct = await genericRepository.GetEntityById(productId);
            if(databaseProduct == null)
            {
                return await Task.FromResult(false);
            }
            if (databaseProduct.SellerId == Int32.Parse(userId) || Int32.Parse(userRole) == 3 || Int32.Parse(userRole) == 2)
            {
                await genericRepository.DeleteEntity(productId);
                return await Task.FromResult(await genericRepository.GetEntityById(productId) == null);
            }
            return await Task.FromResult(false);
        }

        public async Task<IEnumerable<ProductDTO>> GetAllProducts()
        {
            return await mapper.Map<Task<IEnumerable<ProductDTO>>>(genericRepository.GetEntities());
        }

        public async Task<IEnumerable<ProductDTO>> GetAllProducts(string category, string status, double minPrice ,double maxPrice)
        {
            if (maxPrice == 0)
            {
                maxPrice = 999999;
            }

            if (category != null && status != null)
            {
                return await mapper.Map<Task<IEnumerable<ProductDTO>>>(genericRepository.GetEntities(p => p.Status == status && p.Category == category && p.Price < maxPrice && p.Price > minPrice));
            }
            else if (category != null)
            {
                return await mapper.Map<Task<IEnumerable<ProductDTO>>>(genericRepository.GetEntities(p => p.Category == category && p.Price < maxPrice && p.Price > minPrice));
            }
            else if (status != null)
            {
                return await mapper.Map<Task<IEnumerable<ProductDTO>>>(genericRepository.GetEntities(p => p.Status == status && p.Price < maxPrice && p.Price > minPrice));
            } 
            return await mapper.Map<Task<IEnumerable<ProductDTO>>>(genericRepository.GetEntities(p =>  p.Price < maxPrice && p.Price > minPrice));
        }

        public async Task<IEnumerable<ProductDTO>> GetMyProducts(string userId)
        {
            return await mapper.Map<Task<IEnumerable<ProductDTO>>>(genericRepository.GetEntities(p => p.SellerId == Int32.Parse(userId)));
        }

        public async Task<ProductDTO> GetById(int productId)
        {
            return await mapper.Map<Task<ProductDTO>>(genericRepository.GetEntityById(productId));
        }

        public async Task<int> Insert(ProductDTO product)
        {
            product.CreateDate = DateTime.Now;
            if(product.Quantity == 0)
            {
                product.Status = "Inactive";
            }
            Product entityProduct = mapper.Map<Product>(product);
            await genericRepository.InsertEntity(entityProduct);
            return await Task.FromResult(entityProduct.ProductId);
        }

        public async Task<bool> Update(ProductDTO product, string userId, string userRole)
        {
            Product entityProduct = mapper.Map<Product>(product);
            Product databaseProduct = await genericRepository.GetEntityById(product.ProductId);
            if (databaseProduct.SellerId == Int32.Parse(userId) || Int32.Parse(userRole) == 3 || Int32.Parse(userRole) == 2)
            {
                if (Int32.Parse(userRole) == 1)
                {                    
                    entityProduct.Status = databaseProduct.Status;                    
                }
                if (entityProduct.Quantity == 0)
                {
                    entityProduct.Status = "Inactive";
                }
                await genericRepository.UpdateEntity(entityProduct, (int)product.ProductId);
                return await Task.FromResult(entityProduct.ProductId != 0);
            }
            return await Task.FromResult(false);
        }
    }
}
