﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using PlasticFreeLife.DAL;
using PlasticFreeLife.DTO;
using PlasticFreeLife.Models;

namespace PlasticFreeLife.Services
{
    public class FriendsService : IFriendsService
    {
        internal IGenericRepository<Friend> genericRepository;
        internal IMapper mapper;
        internal IAuthentificate authenthificate;

        public FriendsService(IMapper mapper, IGenericRepository<Friend> genericRepository)
        {
            this.genericRepository = genericRepository;
            this.mapper = mapper;
        }

        public async Task<bool> Delete(FriendDTO friend, string userId, string userRole)
        {
            if ( friend.Friend1Id == Int32.Parse(userId) || friend.Friend2Id == Int32.Parse(userId) || Int32.Parse(userRole) == 3 || Int32.Parse(userRole) == 2)
            {
                await genericRepository.DeleteEntity(friend.Friend1Id, friend.Friend2Id);
                return await Task.FromResult(await genericRepository.GetEntityById(friend.Friend1Id, friend.Friend2Id) == null);
            }
            return await Task.FromResult(false);
        }

        public async Task<IEnumerable<FriendDTO>> GetAllFriends()
        {
            return await mapper.Map<Task<IEnumerable<FriendDTO>>>(genericRepository.GetEntities());
        }

        public async Task<IEnumerable<FriendDTO>> GetMyFriends(string userId)
        {
            return await mapper.Map<Task<IEnumerable<FriendDTO>>>(genericRepository.GetEntities(p => p.Friend1Id == Int32.Parse(userId) || p.Friend2Id == Int32.Parse(userId)));
        }

        public async Task<bool> Insert(FriendDTO friend)
        {
            Friend entityFriend = mapper.Map<Friend>(friend);
            await genericRepository.InsertEntity(entityFriend);
            return await Task.FromResult(await genericRepository.GetEntityById(friend.Friend1Id, friend.Friend2Id) != null);
        }
    }
}
