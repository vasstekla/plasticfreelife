﻿using Microsoft.EntityFrameworkCore;
using PlasticFreeLife.Models;
using PlasticFreeLife.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlasticFreeLife.Context
{
    public class DatabaseInitializer
    {
        private ModelBuilder modelBuilder { get; set; }

        public DatabaseInitializer(ModelBuilder modelBuilder)
        {
            this.modelBuilder = modelBuilder;
        }

        public void Seed()
        {
            SeedRoles();
            SeedUsers();
        }

        private void SeedRoles()
        {            
            modelBuilder.Entity<Role>()
                          .HasData(
                                  new Role
                                  {
                                      RoleId = 3,
                                      Name = "admin"
                                  },
                                  new Role
                                  {
                                      RoleId = 2,
                                      Name = "manager"
                                  },
                                  new Role
                                  {
                                      RoleId = 1,
                                      Name = "user"
                                  }
                          );
        }

        private void SeedUsers()
        {
            Authentificate authentificate = new Authentificate();
            String adminPassword = authentificate.HashPassword("admin");
            String managerPassword = authentificate.HashPassword("manager");
            String userPassword = authentificate.HashPassword("user");
            modelBuilder.Entity<User>()
                          .HasData(
                                  new User
                                  {
                                      UserId = 3,
                                      FirstName = "Admin",
                                      LastName = "Admin",
                                      EmailAddress = "admin@plasticfreelife.org",
                                      Username = "admin",
                                      Password = adminPassword,
                                      RoleId = 3
                                  },
                                  new User
                                  {
                                      UserId = 2,
                                      FirstName = "Manager",
                                      LastName = "Manager",
                                      EmailAddress = "manager@plasticfreelife.org",
                                      Username = "manager",
                                      Password = managerPassword,
                                      RoleId = 2
                                  },
                                  new User
                                  {
                                      UserId = 1,
                                      FirstName = "User",
                                      LastName = "User",
                                      EmailAddress = "user@plasticfreelife.org",
                                      Username = "user",
                                      Password = userPassword,
                                      RoleId = 1
                                  }
                          );
        }
    }
}
