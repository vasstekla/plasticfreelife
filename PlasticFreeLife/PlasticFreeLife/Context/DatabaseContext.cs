﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PlasticFreeLife.Models;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PlasticFreeLife.Context
{
    public class DatabaseContext : DbContext
    {
        public DbSet<User> User { get; set; }
        public DbSet<Role> Role { get; set; }
        public DbSet<Post> Post { get; set; }
        public DbSet<Purchase> Purchase { get; set; }
        public DbSet<Product> Product { get; set; }

        public DatabaseContext() { }

        public DatabaseContext(DbContextOptions<DatabaseContext> options)
        : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Purchase>().HasKey(p => new { p.PurchaseId });
            modelBuilder.Entity<Purchase>()
                .HasOne<Product>(p => p.Product)
                .WithMany(p => p.Purchases)
                .HasForeignKey(p => p.ProductId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Purchase>()
                .HasOne<User>(p => p.Buyer)
                .WithMany(p => p.Purchases)
                .HasForeignKey(p => p.BuyerId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Post>().HasKey(p => new { p.PostId });
            modelBuilder.Entity<Post>()
                .HasOne<User>(p => p.Author)
                .WithMany(p => p.Posts)
                .HasForeignKey(p => p.AuthorId);

            modelBuilder.Entity<Product>().HasKey(p => new { p.ProductId });
            modelBuilder.Entity<Product>()
                .HasOne<User>(p => p.Seller)
                .WithMany(p => p.Products)
                .HasForeignKey(p => p.SellerId);

            modelBuilder.Entity<User>().HasKey(p => new { p.UserId });
            modelBuilder.Entity<User>()
                .HasOne<Role>(r => r.Role)
                .WithMany(r => r.Users)
                .HasForeignKey(r => r.RoleId);

            modelBuilder.Entity<Friend>().HasKey(f => new { f.Friend1Id, f.Friend2Id });
            modelBuilder.Entity<Friend>()
                .HasOne<User>(f => f.Friend1)
                .WithMany()
                .HasForeignKey(f => f.Friend1Id)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Friend>()
                .HasOne<User>(f => f.Friend2)
                .WithMany()
                .HasForeignKey(f => f.Friend2Id)
                .OnDelete(DeleteBehavior.Restrict);

            DatabaseInitializer databaseInitializer = new DatabaseInitializer(modelBuilder);
            databaseInitializer.Seed();
        }

    }
}
