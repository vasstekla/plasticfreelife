﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PlasticFreeLife.Context;
using PlasticFreeLife.DTO;
using PlasticFreeLife.Models;
using PlasticFreeLife.Services;

namespace PlasticFreeLife.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUsersService usersService;
        private readonly IAuthentificate authentificate;
        private readonly AppSettings appSettings;
        private readonly ILogger logger;

        public UsersController(IUsersService usersService, IOptions<AppSettings> appSettings, IAuthentificate authentificate, ILogger<UsersController> logger)
        {
            this.usersService = usersService;
            this.appSettings = appSettings.Value;
            this.authentificate = authentificate;
            this.logger = logger;
        }

        // GET: api/Users
        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserDTO>>> GetUsers()
        {
            logger.LogInformation("Getting users");
            if (authentificate.UnwrapTokenRole(Request.Headers["Authorization"].ToString().Split(' ')[1], Encoding.ASCII.GetBytes(appSettings.Secret)) == "3")
            {
                IEnumerable<UserDTO> usersList = await usersService.GetAllUsers();
                if (usersList.Count() == 0)
                {
                    logger.LogWarning("No users were found.");
                }
                return new ObjectResult(usersList);
            }
            logger.LogError("Not authorized.");
            return Unauthorized();
        }

        // GET: api/Users/idValue
        [HttpGet("{id}")]
        public async Task<ActionResult<UserDTO>> GetUser(int id)
        {
            var token = Request.Headers["Authorization"].ToString();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            if (token != "")
            {
                token = token.Split(' ')[1];
                if (authentificate.UnwrapTokenRole(token, key) == "3" ||
                  authentificate.UnwrapTokenName(token, key) == id.ToString())
                {
                    logger.LogInformation("Getting user {ID}", id);
                    UserDTO user = await usersService.GetById(id);
                    if (user == null)
                    {
                        logger.LogWarning("User not found.");
                        return NotFound();
                    }
                    return new ObjectResult(user);
                }
            }
            return Unauthorized();
        }

        // PUT: api/Users
        [HttpPut]
        public async Task<IActionResult> PutUser(UserDTO user)
        {
            var token = Request.Headers["Authorization"].ToString();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            if (token != "")
            {
                token = token.Split(' ')[1];
                if (authentificate.UnwrapTokenRole(token, key) == "3" ||
                  authentificate.UnwrapTokenName(token, key) == user.UserId.ToString())
                {
                    logger.LogInformation("Updating user {ID}", user.UserId);
                    bool isUpdated = await usersService.Update(user);
                    if (isUpdated)
                    {
                        return NoContent();
                    }
                    logger.LogError("Couldn't update user object.");
                    return StatusCode(500);
                }
            }
            return Unauthorized();
        }

        // POST: api/Users
        [HttpPost]
        public async Task<ActionResult<UserDTO>> PostUser(UserDTO user)
        {
            logger.LogInformation("Adding post");
            int userId = await usersService.InsertAsync(user);
            if (userId == -1)
            {
                return new ObjectResult("The username already exists.");
            }
            if (userId != 0)
            {
                return Created("User created", user);
            }
            logger.LogError("Couldn't create user object.");
            return StatusCode(500);
        }

        // POST: api/Users/login
        [HttpPost("login")]
        public async Task<ActionResult<UserDTO>> AuthenticateUser(AuthenticateUserDTO user)
        {
            logger.LogInformation("Logging user");
            UserDTO userLogin = await usersService.Authenticate(user);
            if (userLogin == null)
            {
                logger.LogWarning("User couldn't log in.");
                return BadRequest();
            }
            return new ObjectResult(userLogin.Token);
        }

        // DELETE: api/Users/idValue
        [HttpDelete("{id}")]
        public async Task<ActionResult<UserDTO>> DeleteUser(int id)
        {
            logger.LogInformation("Deleting user");
            var token = Request.Headers["Authorization"].ToString();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            if (token != "")
            {
                token = token.Split(' ')[1];
                if (authentificate.UnwrapTokenRole(token, key) == "3" ||
                  authentificate.UnwrapTokenName(token, key) == id.ToString())
                {
                    bool isDeleted = await usersService.Delete(id);
                    if (isDeleted)
                    {
                        return Ok("Deleted");
                    }
                    logger.LogError("Couldn't delete user object.");
                    return NotFound();
                }
            }
            return Unauthorized();
        }
    }
}
