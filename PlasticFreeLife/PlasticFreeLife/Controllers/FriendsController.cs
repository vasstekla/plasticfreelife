﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PlasticFreeLife.DTO;
using PlasticFreeLife.Services;

namespace PlasticFreeLife.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FriendsController : ControllerBase
    {
        private readonly IFriendsService friendsService;
        private readonly IAuthentificate authentificate;
        private readonly AppSettings appSettings;
        private readonly ILogger logger;

        public FriendsController(IFriendsService friendsService, IOptions<AppSettings> appSettings, IAuthentificate authentificate, ILogger<FriendsController> logger)
        {
            this.friendsService = friendsService;
            this.appSettings = appSettings.Value;
            this.authentificate = authentificate;
            this.logger = logger;
        }

        // GET: api/Friends
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FriendDTO>>> GetFriends()
        {
            logger.LogInformation("Getting friends");
            if (authentificate.UnwrapTokenRole(Request.Headers["Authorization"].ToString().Split(' ')[1], Encoding.ASCII.GetBytes(appSettings.Secret)) == "3")
            {
                IEnumerable<FriendDTO> fiendsList = await friendsService.GetAllFriends();
                if (fiendsList.Count() == 0)
                {
                    logger.LogWarning("No friends were found.");
                }
                return new ObjectResult(fiendsList);
            }
            logger.LogError("Not authorized.");
            return Unauthorized();
        }

        // GET: api/Friends/MyFriends
        [HttpGet("MyFriends")]
        public async Task<ActionResult<IEnumerable<FriendDTO>>> GetMyFriends()
        {
            logger.LogInformation("Getting your friends");
            IEnumerable<FriendDTO> fiendsList = await friendsService.GetMyFriends(authentificate.UnwrapTokenName(Request.Headers["Authorization"].ToString().Split(' ')[1], Encoding.ASCII.GetBytes(appSettings.Secret)));
            if (fiendsList.Count() == 0)
            {
                logger.LogWarning("You don't have any friends.");
            }
            return new ObjectResult(fiendsList);
        }

        // POST: api/Friends
        [HttpPost]
        public async Task<ActionResult<bool>> PostFriend(FriendDTO friend)
        {
            logger.LogInformation("Adding friend");
            var token = Int32.Parse(authentificate.UnwrapTokenName(Request.Headers["Authorization"].ToString().Split(' ')[1], Encoding.ASCII.GetBytes(appSettings.Secret)));
            if (token == friend.Friend1Id || token == friend.Friend2Id)
            {
                bool isInserted = await friendsService.Insert(friend);
                if(isInserted)
                {
                    return Created("Friend entry created", friend);
                }
                logger.LogError("Couldn't create friend object.");
                return StatusCode(500);
            }
            return Unauthorized();
        }

        // DELETE: api/Friends
        [HttpDelete]
        public async Task<ActionResult<FriendDTO>> DeleteProduct(FriendDTO friend)
        {
            logger.LogInformation("Deleting friend");
            bool isDeleted = await friendsService.Delete(friend, authentificate.UnwrapTokenName(Request.Headers["Authorization"].ToString().Split(' ')[1], Encoding.ASCII.GetBytes(appSettings.Secret)), authentificate.UnwrapTokenRole(Request.Headers["Authorization"].ToString().Split(' ')[1], Encoding.ASCII.GetBytes(appSettings.Secret)));
            if (isDeleted)
            {
                return Ok("Deleted");
            }
            logger.LogError("Couldn't delete friend object.");
            return StatusCode(500);
        }

    }
}