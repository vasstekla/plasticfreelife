﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PlasticFreeLife.DTO;
using PlasticFreeLife.Models;
using PlasticFreeLife.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlasticFreeLife.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostsController : ControllerBase
    {
        private readonly IPostsService postsService;
        private readonly IAuthentificate authentificate;
        private readonly AppSettings appSettings;
        private readonly ILogger logger;

        public PostsController(IPostsService postsService, IOptions<AppSettings> appSettings, IAuthentificate authentificate, ILogger<PostsController> logger)
        {
            this.postsService = postsService;
            this.appSettings = appSettings.Value;
            this.authentificate = authentificate;
            this.logger = logger;
        }

        // GET: api/Posts
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PostDTO>>> GetPosts()
        {
            logger.LogInformation("Getting posts");
            IEnumerable<PostDTO> postsList = await postsService.GetAllPosts();
            if (postsList.Count() == 0)
            {
                logger.LogWarning("No posts were found.");
                return NotFound();
            }
            return new ObjectResult(postsList);
        }

        // GET: api/Posts/getFilteredPosts?category=categoryValue&&status=statusValue
        [HttpGet("getFilteredPosts")]
        public async Task<ActionResult<IEnumerable<PostDTO>>> GetFilteredPosts([FromQuery(Name = "category")] string category, [FromQuery(Name = "status")] string status)
        {
            logger.LogInformation("Getting filtered posts");
            IEnumerable<PostDTO> postsList = await postsService.GetAllPosts(category, status);
            if (postsList.Count() == 0)
            {
                logger.LogWarning("No posts were found that match the filters.");
                return NotFound();
            }
            return new ObjectResult(postsList);
        }

        // GET: api/Posts/idNumber
        [HttpGet("{id}")]
        public async Task<ActionResult<PostDTO>> GetPost(int id)
        {
            logger.LogInformation("Getting post {ID}", id);
            PostDTO post = await postsService.GetById(id);
            if (post != null)
            {
                logger.LogWarning("Post not found.");
                return NotFound();
            }
            return new ObjectResult(post);
        }

        // PUT: api/Posts
        [HttpPut]
        public async Task<IActionResult> PutPost(PostDTO post)
        {
            logger.LogInformation("Updating post {ID}", post.PostId);
            bool isUpdated = await postsService.Update(post, authentificate.UnwrapTokenName(Request.Headers["Authorization"].ToString().Split(' ')[1], Encoding.ASCII.GetBytes(appSettings.Secret)), authentificate.UnwrapTokenRole(Request.Headers["Authorization"].ToString().Split(' ')[1], Encoding.ASCII.GetBytes(appSettings.Secret)));
            if (isUpdated)
            {
                return NoContent();
            }
            logger.LogError("Couldn't update post object.");
            return StatusCode(500);
        }

        // POST: api/Posts
        [HttpPost]
        public async Task<ActionResult<Post>> PostPost(PostDTO post)
        {
            logger.LogInformation("Adding post");
            post.AuthorId = Int32.Parse(authentificate.UnwrapTokenName(Request.Headers["Authorization"].ToString().Split(' ')[1], Encoding.ASCII.GetBytes(appSettings.Secret)));
            int postId = await postsService.Insert(post, authentificate.UnwrapTokenRole(Request.Headers["Authorization"].ToString().Split(' ')[1], Encoding.ASCII.GetBytes(appSettings.Secret)));
            if (postId != 0)
            {
                return Created("Post entry created", post);
            }
            logger.LogError("Couldn't create post object.");
            return StatusCode(500);
        }

        // DELETE: api/Posts/idNumber
        [HttpDelete("{id}")]
        public async Task<ActionResult<PostDTO>> DeletePost(int id)
        {
            logger.LogInformation("Deleting post");
            bool isDeleted = await postsService.Delete(id, authentificate.UnwrapTokenName(Request.Headers["Authorization"].ToString().Split(' ')[1], Encoding.ASCII.GetBytes(appSettings.Secret)), authentificate.UnwrapTokenRole(Request.Headers["Authorization"].ToString().Split(' ')[1], Encoding.ASCII.GetBytes(appSettings.Secret)));
            if (isDeleted)
            {
                return Ok("Deleted");
            }
            logger.LogError("Couldn't delete post object.");
            return StatusCode(500);
        }
    }
}
