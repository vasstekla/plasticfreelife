﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PlasticFreeLife.DTO;
using PlasticFreeLife.Services;

namespace PlasticFreeLife.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PurchasesController : ControllerBase
    {
        private readonly IPurchasesService purchasesService;
        private readonly IAuthentificate authentificate;
        private readonly AppSettings appSettings;
        private readonly ILogger logger;

        public PurchasesController(IPurchasesService purchasesService, IOptions<AppSettings> appSettings, IAuthentificate authentificate, ILogger<PurchasesController> logger)
        {
            this.purchasesService = purchasesService;
            this.appSettings = appSettings.Value;
            this.authentificate = authentificate;
            this.logger = logger;
        }

        // GET: api/Purchases
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PurchaseDTO>>> GetPurchases()
        {
            logger.LogInformation("Getting puchases");
            if (authentificate.UnwrapTokenRole(Request.Headers["Authorization"].ToString().Split(' ')[1], Encoding.ASCII.GetBytes(appSettings.Secret)) == "3")
            {
                IEnumerable<PurchaseDTO> purchasesList = await purchasesService.GetAllPurchases();
                if (purchasesList.Count() == 0)
                {
                    logger.LogWarning("No purchases were found.");
                }
                return new ObjectResult(purchasesList);
            }
            logger.LogError("Not authorized.");
            return Unauthorized();
        }

        // GET: api/Purchases/MyPurchases
        [HttpGet("MyPurchases")]
        public async Task<ActionResult<IEnumerable<PurchaseDTO>>> GetMyPurchases()
        {
            logger.LogInformation("Getting user's purchases");
            IEnumerable<PurchaseDTO> purchasesList = await purchasesService.GetMyPurchases(authentificate.UnwrapTokenName(Request.Headers["Authorization"].ToString().Split(' ')[1], Encoding.ASCII.GetBytes(appSettings.Secret)));
            if (purchasesList.Count() == 0)
            {
                logger.LogWarning("No purchases of the user were found.");
            }
            return new ObjectResult(purchasesList);
        }

        // GET: api/Purchases/idValue
        [HttpGet("{id}")]
        public async Task<ActionResult<PurchaseDTO>> GetPurchase(int id)
        {
            logger.LogInformation("Getting purchase {ID}", id);
            if (authentificate.UnwrapTokenRole(Request.Headers["Authorization"].ToString().Split(' ')[1], Encoding.ASCII.GetBytes(appSettings.Secret)) == "3")
            {
                PurchaseDTO purchase = await purchasesService.GetById(id);
                if (purchase != null)
                {
                    logger.LogWarning("Purchase not found.");
                    return NotFound();
                }
                return new ObjectResult(purchase);
            }
            return Unauthorized();
        }


        // POST: api/Purchases
        [HttpPost]
        public async Task<ActionResult<PurchaseDTO>> PostPurchase(PurchaseDTO purchase)
        {
            logger.LogInformation("Adding purchase");
            purchase.BuyerId = Int32.Parse(authentificate.UnwrapTokenName(Request.Headers["Authorization"].ToString().Split(' ')[1], Encoding.ASCII.GetBytes(appSettings.Secret)));
            int purchaseId = await purchasesService.Insert(purchase);
            if (purchaseId != 0)
            {
                return Created("Purchase entry created", purchase);
            }
            logger.LogError("Couldn't create purchase object.");
            return StatusCode(500);
        }

    }
}