﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PlasticFreeLife.DTO;
using PlasticFreeLife.Services;

namespace PlasticFreeLife.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly IProductsService productsService;
        private readonly IAuthentificate authentificate;
        private readonly AppSettings appSettings;
        private readonly ILogger logger;

        public ProductsController(IProductsService productsService, IOptions<AppSettings> appSettings, IAuthentificate authentificate, ILogger<ProductsController> logger)
        {
            this.productsService = productsService;
            this.appSettings = appSettings.Value;
            this.authentificate = authentificate;
            this.logger = logger;

        }

        // GET: api/Products
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProductDTO>>> GetProducts()
        {
            logger.LogInformation("Getting products");
            IEnumerable<ProductDTO> productsList = await productsService.GetAllProducts();
            if (productsList.Count() == 0)
            {
                logger.LogWarning("No products were found.");
            }
            return new ObjectResult(productsList);
        }

        // GET: api/Products/Products?category=categoryValue&&status=statusValue&&minPrice=minPriceValue&&maxPrice=maxPriceValue
        [HttpGet("Products")]
        public async Task<ActionResult<IEnumerable<ProductDTO>>> GetFilteredProducts([FromQuery(Name = "category")] string category, [FromQuery(Name = "status")] string status, [FromQuery(Name = "minPrice")] double minPrice, [FromQuery(Name = "maxPrice")] double maxPrice)
        {
            logger.LogInformation("Getting filtered product");
            IEnumerable<ProductDTO> productsList = await productsService.GetAllProducts(category, status, minPrice, maxPrice);
            if (productsList.Count() == 0)
            {
                logger.LogWarning("No products were found that match the filters.");
            }
            return new ObjectResult(productsList);
        }

        // GET: api/Products/MyProducts
        [HttpGet("MyProducts")]
        public async Task<ActionResult<IEnumerable<ProductDTO>>> GetMyProductsProducts()
        {
            logger.LogInformation("Getting user's products");
            IEnumerable<ProductDTO> productsList = await productsService.GetMyProducts(authentificate.UnwrapTokenName(Request.Headers["Authorization"].ToString().Split(' ')[1], Encoding.ASCII.GetBytes(appSettings.Secret)));
            if (productsList.Count() == 0)
            {
                logger.LogWarning("No products of the user were found.");
            }
            return new ObjectResult(productsList);
        }

        // GET: api/Products/idValue
        [HttpGet("{id}")]
        public async Task<ActionResult<ProductDTO>> GetProduct(int id)
        {
            logger.LogInformation("Getting product {ID}", id);
            ProductDTO product = await productsService.GetById(id);
            if (product == null)
            {
                logger.LogWarning("Product not found.");
                return NotFound();
            }
            return new ObjectResult(product);
        }

        // PUT: api/Products
        [HttpPut]
        public async Task<IActionResult> PutProduct(ProductDTO product)
        {
            logger.LogInformation("Updating product {ID}", product.ProductId);
            bool isUpdated = await productsService.Update(product, authentificate.UnwrapTokenName(Request.Headers["Authorization"].ToString().Split(' ')[1], Encoding.ASCII.GetBytes(appSettings.Secret)), authentificate.UnwrapTokenRole(Request.Headers["Authorization"].ToString().Split(' ')[1], Encoding.ASCII.GetBytes(appSettings.Secret)));
            if (isUpdated)
            {
                return NoContent();
            }
            logger.LogError("Couldn't update product object.");
            return StatusCode(500);
        }

        // POST: api/Products
        [HttpPost]
        public async Task<ActionResult<ProductDTO>> PostProduct(ProductDTO product)
        {
            logger.LogInformation("Adding product");
            product.SellerId = Int32.Parse(authentificate.UnwrapTokenName(Request.Headers["Authorization"].ToString().Split(' ')[1], Encoding.ASCII.GetBytes(appSettings.Secret)));
            int productId = await productsService.Insert(product);
            product.ProductId = productId;
            if (productId != 0)
            {
                return Created("Product entry created", product);
            }
            logger.LogError("Couldn't create product object.");
            return StatusCode(500);
        }

        // DELETE: api/Products/idValue
        [HttpDelete("{id}")]
        public async Task<ActionResult<ProductDTO>> DeleteProduct(int id)
        {
            logger.LogInformation("Deleting product");
            bool isDeleted = await productsService.Delete(id, authentificate.UnwrapTokenName(Request.Headers["Authorization"].ToString().Split(' ')[1], Encoding.ASCII.GetBytes(appSettings.Secret)), authentificate.UnwrapTokenRole(Request.Headers["Authorization"].ToString().Split(' ')[1], Encoding.ASCII.GetBytes(appSettings.Secret)));
            if (isDeleted)
            {
                return Ok("Deleted");
            }
            logger.LogError("Couldn't delete product object.");
            return NotFound();
        }
    }
}