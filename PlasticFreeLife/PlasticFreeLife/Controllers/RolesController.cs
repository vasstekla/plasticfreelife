﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PlasticFreeLife.Context;
using PlasticFreeLife.DTO;
using PlasticFreeLife.Models;
using PlasticFreeLife.Services;

namespace PlasticFreeLife.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RolesController : ControllerBase
    {
        private readonly IRolesService rolesService;
        private readonly IAuthentificate authentificate;
        private readonly AppSettings appSettings;
        private readonly ILogger logger;

        public RolesController(IRolesService rolesService, IOptions<AppSettings> appSettings, IAuthentificate authentificate, ILogger<RolesController> logger)
        {
            this.rolesService = rolesService;
            this.appSettings = appSettings.Value;
            this.authentificate = authentificate;
            this.logger = logger;
        }

        // GET: api/Roles
        [HttpGet]
        public async Task<ActionResult<IEnumerable<RoleDTO>>> GetRoles()
        {
            logger.LogInformation("Getting roles");
            if (authentificate.UnwrapTokenRole(Request.Headers["Authorization"].ToString().Split(' ')[1], Encoding.ASCII.GetBytes(appSettings.Secret)) == "3")
            {
                IEnumerable<RoleDTO> rolesList = await rolesService.GetAllRoles();
                if (rolesList.Count() == 0)
                {
                    logger.LogWarning("No roles were found.");
                }
                return new ObjectResult(rolesList);
            }
            logger.LogError("Not authorized.");
            return Unauthorized();
        }

        // GET: api/Roles/idValue
        [HttpGet("{id}")]
        public async Task<ActionResult<RoleDTO>> GetRole(int id)
        {
            logger.LogInformation("Getting role {ID}", id);
            RoleDTO role = await rolesService.GetById(id);
            if (role == null)
            {
                logger.LogWarning("Role not found.");
                return NotFound();
            }
            return new ObjectResult(role);
        }

        // PUT: api/Roles
        [HttpPut]
        public async Task<IActionResult> PutRole(RoleDTO role)
        {
            logger.LogInformation("Updating role {ID}", role.RoleId);
            bool isUpdated = await rolesService.Update(role);
            if (isUpdated)
            {
                return NoContent();
            }
            logger.LogError("Couldn't update role object.");
            return StatusCode(500);
        }

        // POST: api/Roles
        [HttpPost]
        public async Task<ActionResult<Role>> PostRole(RoleDTO role)
        {
            logger.LogInformation("Adding role");
            int roleId = await rolesService.Insert(role);
            role.RoleId = roleId;
            if (roleId != 0)
            {
                return Created("Role created", role);
            }
            logger.LogError("Couldn't create role object.");
            return StatusCode(500);
        }

        // DELETE: api/Roles/idValue
        [HttpDelete("{id}")]
        public async Task<ActionResult<RoleDTO>> DeleteRole(int id)
        {
            logger.LogInformation("Deleting role");
            bool isDeleted = await rolesService.Delete(id);
            if (isDeleted)
            {
                return Ok("Deleted");
            }
            logger.LogError("Couldn't delete role object.");
            return StatusCode(500);
        }
    }
}
