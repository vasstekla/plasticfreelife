﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlasticFreeLife.DTO
{
    public class FriendDTO
    {
        public int Friend1Id { get; set; }
        public int Friend2Id { get; set; }
    }
}
