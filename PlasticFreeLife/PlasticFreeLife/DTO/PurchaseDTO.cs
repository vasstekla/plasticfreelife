﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlasticFreeLife.DTO
{
    public class PurchaseDTO
    {
        public int PurchaseId { get; set; }
        public int ProductId { get; set; }
        public int BuyerId { get; set; }
        public DateTime DateTime { get; set; }
    }
}
