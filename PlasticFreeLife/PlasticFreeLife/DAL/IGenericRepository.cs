﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace PlasticFreeLife.DAL
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        Task<IEnumerable<TEntity>> GetEntities(Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "");

        Task<TEntity> GetEntityById(object entityId);
        Task<TEntity> GetEntityById(object entityId1, object entityId2);
        Task InsertEntity(TEntity entity);
        Task DeleteEntity(object entityId);
        Task DeleteEntity(object entityId1, object entityId2);
        Task UpdateEntity(TEntity entity, object entityId);
    }
}
