﻿using Microsoft.EntityFrameworkCore;
using PlasticFreeLife.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace PlasticFreeLife.DAL
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        internal DatabaseContext context;
        internal DbSet<TEntity> dbSet;

        public GenericRepository(DatabaseContext context)
        {
            this.context = context;
            this.dbSet = context.Set<TEntity>();
        }

        public virtual async Task<IEnumerable<TEntity>> GetEntities(Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "")
        {
            IQueryable<TEntity> query = dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                return await orderBy(query).ToListAsync();
            }
            else
            {
                return await query.ToListAsync();
            }
        }

        public virtual async Task<TEntity> GetEntityById(object entityId)
        {
            return await dbSet.FindAsync(entityId);
        }

        public virtual async Task<TEntity> GetEntityById(object entityId1, object entityId2)
        {
            return await dbSet.FindAsync(entityId1, entityId2);
        }

        public async Task InsertEntity(TEntity entity)
        {
            await dbSet.AddAsync(entity);
            await SaveAsync();
        }

        public async Task DeleteEntity(object entityId)
        {
            TEntity entity = await dbSet.FindAsync(entityId);
            dbSet.Remove(entity);
            await SaveAsync();
        }

        public async Task DeleteEntity(object entityId1, object entityId2)
        {
            TEntity entity = await dbSet.FindAsync(entityId1, entityId2);
            dbSet.Remove(entity);
            await SaveAsync();
        }

        public async Task UpdateEntity(TEntity entity, object entityId)
        {
            TEntity previousEntity = await dbSet.FindAsync(entityId);
            context.Entry(previousEntity).CurrentValues.SetValues(entity);
            await SaveAsync();
        }

        public async Task SaveAsync()
        {
            await this.context.SaveChangesAsync();
        }
    }
}
