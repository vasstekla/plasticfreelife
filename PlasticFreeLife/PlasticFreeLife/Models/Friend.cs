﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlasticFreeLife.Models
{
    public class Friend
    {
        public User Friend1 { get; set; }
        public int Friend1Id { get; set; }
        public User Friend2 { get; set; }
        public int Friend2Id { get; set; }
    }
}
