﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlasticFreeLife.Models
{
    public class Purchase
    {
        public int PurchaseId { get; set; }
        public Product Product { get; set; }
        public int ProductId { get; set; }
        public User Buyer { get; set; }
        public int BuyerId { get; set; }
        public DateTime DateTime { get; set; }
    }
}
