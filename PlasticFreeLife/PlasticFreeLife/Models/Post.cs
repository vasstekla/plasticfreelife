﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlasticFreeLife.Models
{
  public class Post
  {
    public int PostId { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public string Content { get; set; }
    public string Category { get; set; }
    public DateTime CreateDate { get; set; }
    public string Status { get; set; }
    public User Author { get; set; }
    public int AuthorId { get; set; }
  }
}
