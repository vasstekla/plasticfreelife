﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlasticFreeLife.Models
{
    public class Product
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public int Quantity { get; set; }
        public DateTime CreateDate { get; set; }
        public string Status { get; set; }
        public double Price { get; set; }
        public User Seller { get; set; }
        public int SellerId { get; set; }
        public ICollection<Purchase> Purchases { get; set; }
    }
}
