﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlasticFreeLife.Models
{
    public class User
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
        public Role Role { get; set; }
        public int RoleId { get; set; }
        public ICollection<Post> Posts { get; set; }
        public ICollection<Product> Products { get; set; }
        public ICollection<Purchase> Purchases { get; set; }
    }
}
