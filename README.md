# PlasticFreeLife

# Download and install:
1. **SQL server** (free version)
2. Microsoft SQL server Management studio (optional -- here you can easily find your SQL server's name)
2. **.Net**
3. Postman (optional)
# Start:
1. **Clone** the repository  
2. Add your **SQL server name** to plasticfreelife\PlasticFreeLife\PlasticFreeLife\appsettings.json at   
"ConnectionStrings": {"PlasticFreeLife": "**Server=L-PF081FR5**;Database=PlasticFreeLife;Trusted_Connection=True;"} 
3. **Update database**: `dotnet ef database update --project .\PlasticFreeLife\PlasticFreeLife`
4. **Build** the project: `dotnet build .\PlasticFreeLife`
5. **Run** the project:`dotnet run --project .\PlasticFreeLife\PlasticFreeLife`
6. Go to the listed port with your **browser or postman**.


# Note:
Most of the API calls need authentication. To try some of the functionalities out, first acces `http://localhost:PORTNUMBER/api/Users/login`, and add the following to the body:  
"username" : "**user**"  
"password" : "**user**"  
OR for full rights   
"username" : "**admin**"  
"password" : "**admin**"  
The result will contain a generated token, which needs to be added to the next calls, doing the following:  
1. In postman click on Authorization.
2. From types select Bearer Token.
3. Copy the token from the login response and paste to the token field.

Ready to go. Please note, that some functionalities might be avaible only by accessing them as admin.